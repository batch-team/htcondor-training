#!/usr/bin/env python3

import os
import sys

if len(sys.argv) != 2:
    print(f'Usage: %s DATA {os.path.basename(sys.argv[0])}')
    sys.exit(1)
input_filename = sys.argv[1]

words = {}

my_file = open(input_filename, 'r', encoding='latin-1')
for line in my_file.readlines():
    word = line.strip().lower()
    if word in words:
        words[word] += 1
    else:
        words[word] = 1
my_file.close()

with open('result.txt', 'w') as f:
    for word in sorted(words.keys()):
        print('{:8d} {}'.format(words[word], word), file=f)

print(f'{len(words.keys())} words counted.')
