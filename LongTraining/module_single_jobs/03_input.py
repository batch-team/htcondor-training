#!/usr/bin/env python

import os
import sys

if len(sys.argv) != 2:
    print 'Usage: %s DATA' % (os.path.basename(sys.argv[0]))
    sys.exit(1)
input_filename = sys.argv[1]

words = {}

my_file = open(input_filename, 'r')
for line in my_file:
    word = line.strip().lower()
    if word in words:
        words[word] += 1
    else:
        words[word] = 1
my_file.close()

with open('result.txt', 'w') as f:
    for word in sorted(words.keys()):
        print >> f, '%8d %s' % (words[word], word)

print '%8d words counted.' % (len(words.keys()))
