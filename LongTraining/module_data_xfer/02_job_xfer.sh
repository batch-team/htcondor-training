#!/bin/bash

wget https://bejones.web.cern.ch/bejones/words.txt

sed -e 's/\(.*\)/\U\1/' words.txt > output.txt

mv output.txt /afs/cern.ch/user/b/bejones/tmp/
